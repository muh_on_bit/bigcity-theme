<?php get_header();?>
    <div class="content">
        <div class='title'>
            <h1>News</h1>
            <div>
                <svg width="14px" height="14px">
                    <rect width="18" height="18" style="fill:#FFCD00;" />  
                </svg>
                <a href="<?php echo home_url()?>">All</a>
            </div>
            <div>
                <?php 
                    $cats = get_categories();
                    foreach($cats as $cat){
                ?>
                <svg width="14px" height="14px">
                    <rect width="18" height="18" style="fill:#7F7F7F;" />  
                </svg>
                <a href="<?php echo home_url()."/index.php/category/".$cat->slug;?>"><?php echo $cat->name;?></a>
                <?php }; ?>
            </div>
        </div>
        <div class="News">
            <?php
                $cats = get_categories();
                foreach($cats as $cat){
                    $the_query = new WP_Query( array(
                        'category_name' => $cat->name
                    ));
                    
                    if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ){
                            $the_query->the_post();
                            get_template_part('templates/content', 'postmeta');
                        }
                    }
                }
            ?>
        </div>
    </div>
<?php get_footer();?>
