<div class='post-heading'>
    <h1><?php the_title(); ?></h1>
    <p><?php the_date(); ?></p>
</div>
<div class="blog">
    <img src=<?php the_post_thumbnail_url(); ?> />
    <div class="text">
        <div class="main">
            <?php the_content(); ?>
        </div>
        <div class="sidebar">
            <h1>Related Topics</h1>
            <div class="button-grp">
                <?php the_tags('<button>', '</button><button>', '</button>'); ?>
            </div>
        </div>
    </div>
    <div class="posts">
        <h3>Latest Articles</h3>
        <div class="News">
            <?php
                $the_query = new WP_Query( array(
                    'category_name' => 'news',
                    'posts_per_page' => 3,
                ));
                
                if ( $the_query->have_posts() ) {
                    while ( $the_query->have_posts() ){
                        $the_query->the_post();
                        get_template_part('templates/content', 'postmeta');
                    }
                }
            ?>
        </div>
    </div>
</div>