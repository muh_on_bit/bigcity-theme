<div class="card">
    <div class="ImgDiv"><a href="<?php the_permalink(); ?>"><img src=<?php the_post_thumbnail_url(); ?> /></a></div>
    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <?php the_excerpt(); ?>
</div>