<!DOCTYPE html>
<html>

  <head>
      <title>Test Site</title>
      <?php wp_head(); ?>
  </head>

  <body>
      <div class="container">
        <div class="NavBar">
            <div class="logo">
              <?php the_custom_logo(); ?>
            </div>
            <div class="list">
              <?php
                wp_nav_menu(
                  array(
                    'menu' => 'primary',
                    'theme_location' => 'primary'
                  )
                );
              ?>
                <!-- <ul>
                    <li><a>WORK</a></li>
                    <li><a>ABOUT</a></li>
                    <li><a>NEWS</a></li>
                    <li><a>CONTACT</a></li>
                </ul> -->
            </div>
        </div>