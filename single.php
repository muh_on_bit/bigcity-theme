<?php get_header();?>
    <div class="content">
    <?php
		if ( have_posts() ) {
		
			while ( have_posts() ){

				the_post();
                get_template_part('templates/content', 'articles');

			}
			
		}
    ?>

    </div>

<?php get_footer();?>