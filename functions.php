<?php

    function bigcity_adding_support() {
        add_theme_support('post-thumbnails');
        add_theme_support('custom-logo');
    }

    add_action('after_setup_theme', 'bigcity_adding_support');

    function bigcity_adding_style() {        
        wp_register_style( 'custom-style', get_template_directory_uri() .'/assets/css/custom-style.css', array(), '1.0');

        wp_enqueue_style( 'custom-style' );
    }
    add_action( 'wp_enqueue_scripts', 'bigcity_adding_style' );

    function bigcity_adding_scripts() {   

        wp_enqueue_script( 'bigcity_jquery', 'https://code.jquery.com/jquery-3.6.0.min.js', array(), '3.6');

    }
    add_action( 'wp_enqueue_scripts', 'bigcity_adding_scripts' );

    function bigcity_adding_menus() {
        register_nav_menus(array ('primary' => 'Desktop top menu'));
    }
    add_action( 'init', 'bigcity_adding_menus' );

    function bigcity_adding_widgets() {
        register_sidebar(
            array(
                'before_title'=>'',
                'after_title'=>'',
                'before_widget'=>'',
                'after_widget'=>'',
                'name'=>'Footer Widgets',
                'id'=>'footer-1',
                'description'=>'Footer Widget Area',
            )
            );
    }
    add_action( 'widgets_init', 'bigcity_adding_widgets' );
?>